<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "noticia".
 *
 * @property int $Id
 * @property string|null $Titulo
 * @property string|null $Contenido
 * @property string|null $Autor
 * @property string|null $FechaPublicacion
 */
class Noticia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Id'], 'required'],
            [['Id'], 'integer'],
            [['Contenido'], 'string'],
            [['FechaPublicacion'], 'safe'],
            [['Titulo', 'Autor'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'Titulo' => 'Titulo',
            'Contenido' => 'Contenido',
            'Autor' => 'Autor',
            'FechaPublicacion' => 'Fecha Publicacion',
        ];
    }
}
